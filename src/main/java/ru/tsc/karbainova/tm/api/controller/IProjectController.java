package ru.tsc.karbainova.tm.api.controller;

public interface IProjectController {
    void showProject();

    void clearProject();

    void createProject();
}
