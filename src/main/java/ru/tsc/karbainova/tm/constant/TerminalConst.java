package ru.tsc.karbainova.tm.constant;

public class TerminalConst {
    public static final String CMD_HELP = "help";
    public static final String CMD_VERSION = "version";
    public static final String CMD_ABOUT = "about";
    public static final String CMD_EXIT = "exit";
    public static final String CMD_INFO = "info";
    public static final String CMD_COMMANDS = "commands";
    public static final String CMD_ARGUMENTS = "arguments";
    public static final String CREATE_PROJECT = "create-project";
    public static final String LIST_PROJECT = "list-project";
    public static final String CLEAR_PROJECT = "clear-project";
    public static final String CREATE_TASK = "create-task";
    public static final String LIST_TASK = "list-task";
    public static final String CLEAR_TASK = "clear-task";

}
