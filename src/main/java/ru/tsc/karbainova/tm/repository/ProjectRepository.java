package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {
    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(Project project) {
        projects.add(project);
    }

    @Override
    public void remove(Project project) {
        projects.remove(project);
    }

    public List<Project> findAll() {
        return projects;
    }

    ;

    @Override
    public void clear() {
        projects.clear();
    }
}
